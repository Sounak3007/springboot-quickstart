## This is a sample Java Springboot project.

#Prerequisites 

- Docker Engine/ Docker desktop installed locally. 
- Docker Hub repository.
- openjdk:8 or above image from Docker 
- Maven 
- JDK

**Dckerizing the application** . 

1. Install JDK and Maven in the system.
2. Clone the repository to your local system using git clone <repos url> .
3. Write the Dockerfile for the Java Springboot application.

4. **Dockerfile** :
   

- FROM openjdk:8
- WORKDIR /app
- COPY .mvn/ .mvn
- COPY mvnw pom.xml ./
- RUN ./mvnw dependency:resolve
- COPY src ./src
- CMD ["./mvnw", "spring-boot:run"]

5. **Build the image** 

- docker build -t <image_name> <path of the Dockerfile>

6. Push the docker image to Docker Hub

- docker login
- docker push sounak3007/testjavaimage

6. Run Image as a container:

- Install Docker-Desktop
- Pull the image from the Hub to Local repository
- **Docker run command** 

- docker run -d --name <container_name> -p 8080:8080 <iamge_name>

 -d = for the container to run in the background.
 -p = port mapping. 

7. To view container details , run the command - "docker ps -a"

8. To view images , run the command - "docker images"

9. To test that the application is working prperly, open a new browser and navigate to http://localhost:8080

## Run the container inside the local Kubernetes 

**Prerequisites**

- Install Docker Desktop
- Install Kubernetes in the Docker desktop
- Verify kubernetes cluster 

Steps :

1. Configure Kubernetes deployment yaml file. eg- testdep.yaml

testdep.yaml file:
 
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: testdep
  name: testdep
spec:
  replicas: 3
  selector:
    matchLabels:
      app: testdep
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: testdep
    spec:
      containers:
      - image: sounak3007/testjavaimage
        name: testjavaimage
        ports:
        - containerPort: 8080
          name: http 
status: {}

- Kubectl apply -f testdep.yaml

2. Attch a load balancer to the deployment to access it from the internet . eg lb.yaml

lb.yaml file: 

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: testdep
  name: loadbalancer
spec:
  ports:
  - port: 8080
    protocol: TCP
    targetPort: http
  selector:
    app: testdep
  type: LoadBalancer
status:
  loadBalancer: {}

- kubectl apply -f lb.yaml 







